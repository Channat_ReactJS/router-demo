import React, { Component } from 'react'
import {Route,Link,Switch,Redirect} from "react-router-dom"
import Home from './Home'
import Content from './Content'
function Login(props){
    console.log(props)
    return (
            <div>
                <h1>Login Page</h1>
                <button onClick={()=>props.history.push('/test')}>login</button>
            </div>
        )
}
function Private() {
    return <h1>Private Page</h1>
}

export class Body extends Component {
    constructor(){
        super()
        this.state={
            isAuthenticated:false
        }
    }
  render() {
    return (
      <div>
        <Switch>
            <Route path="/" exact component={Home}/>
            <Route path='/home' component={Home}  />
            <Route path='/menu1' render={()=><h1>Menu1</h1>}  />
            <Route path='/menu2' children={({match})=>(
                                            <li>
                                                <Link style={{color:'red'}} to='/test'>click me</Link>
                                            </li>
            )}  />
            <Route path="/login" component={Login}/>
            <Route path='/menu3' render={()=>
                    this.state.isAuthenticated?<Private/>:<Redirect to="/login"/>
            }/> 
            <Route path='/user/:id' component={Content}/>
            <Route render={()=><h1>404</h1>}/>

        </Switch>
      </div>
    )
  }
}

export default Body
