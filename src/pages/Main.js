import React, { Component } from 'react'
import Header from './Header'
import Body from './Body'
export class Main extends Component {
  render() {
    return (
      <div>
        <Header/>
        <Body/>
      </div>
    )
  }
}

export default Main
